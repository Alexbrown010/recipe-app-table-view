//
//  main.m
//  Recipe app using tableviews
//
//  Created by Alex Brown on 5/25/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
