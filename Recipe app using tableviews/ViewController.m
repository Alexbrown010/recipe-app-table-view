//
//  ViewController.m
//  Recipe app using tableviews
//
//  Created by Alex Brown on 5/25/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayOfRecipies =@[
                       @{
                           @"name":@"Yummy Crepěs", @"image":@"crepe", @"directions":@"In a blender, combine all of the ingredients and pulse for 10 seconds. Place the crepe batter in the refrigerator for 1 hour. This allows the bubbles to subside so the crepes will be less likely to tear during cooking. The batter will keep for up to 48 hours."
                           
                           },
                       @{
                           @"name":@"Tasty Oatmeal", @"image":@"oatmeal", @"directions":@"Chewier: Boil water and salt; stir in oats. Cook about 5 minutes over medium heat, stirring occasionally. \n Creamier: Combine water, oats and salt. Bring to boil. Cook about 5 minutes over medium heat, stirring occasionally."
                           
                           },
                       @{
                           @"name":@"To Die for Waffles", @"image":@"waffles", @"directions":@"2 eggs \n 2 cups all-purpose flour \n 1 3/4 cups milk \n 1/2 cup vegetable oil \n 1 tablespoon white sugar \n 4 teaspoons baking powder \n 1/4 teaspoon salt \n 1/2 teaspoon vanilla extract"
                           
                           },
                       @{
                           @"name":@"Okay Omelette", @"image":@"omelette", @"directions":@"Add your eggs and move the pan around to spread them out evenly. Continue as for the basic omelette. Quarter or roughly chop the mushrooms and add to a hot frying pan with a small knob of butter, a drizzle of olive oil and a pinch of salt and pepper. Fry and toss around until golden, then turn the heat down to medium."
                           
                           },
                       @{
                           @"name":@"French Toast", @"image":@"ft", @"directions":@"Combine Milk and eggs. Dip bread slices and then cook until golden brown. Serve hot with maple syrup"
                           
                           }
                       ];
    
    
    UITableView* tableview = [[UITableView alloc]initWithFrame:self.view.bounds style: UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    [self.view addSubview:tableview];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfRecipies.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
      cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = arrayOfRecipies[indexPath.row];
    
    cell.textLabel.text = [infoDictionary objectForKey:@"name"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = arrayOfRecipies[indexPath.row];
    
    
    infoViewController* info = [infoViewController new];
    info.infoDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
    
}
    

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
