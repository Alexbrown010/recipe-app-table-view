//
//  ViewController.h
//  Recipe app using tableviews
//
//  Created by Alex Brown on 5/25/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoViewController.h"


@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    NSArray* arrayOfRecipies;
}


@end

