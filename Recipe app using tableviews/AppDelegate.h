//
//  AppDelegate.h
//  Recipe app using tableviews
//
//  Created by Alex Brown on 5/25/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

